package com.homebrewsoft.code;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class InfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
    }

    public void clickJoin(View view) {
        Intent intentJoin = new Intent(InfoActivity.this, JoinActivity.class);
        startActivity(intentJoin);
        this.finish();
    }
}
