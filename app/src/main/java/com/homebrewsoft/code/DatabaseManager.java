package com.homebrewsoft.code;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class DatabaseManager {

    private static final String USERS = "users";

    public static Task<Void> setUser(String name, String email) {
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser user = firebaseAuth.getCurrentUser();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference referenceUsers = database.getReference(USERS);
        assert user != null;
        return referenceUsers.child(user.getUid()).setValue(new User(name, email));
    }
}
