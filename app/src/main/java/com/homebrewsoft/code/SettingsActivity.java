package com.homebrewsoft.code;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.util.Log;
import android.util.TypedValue;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.firebase.auth.FirebaseAuth;

import java.io.ByteArrayOutputStream;


public class SettingsActivity extends AppCompatActivity {

    private static final int PICK_IMAGE_REQUEST_CODE = 1;
    private static final int TAKE_PHOTO_REQUEST_CODE = 2;

    FirebaseAuth firebaseAuth;

    ImageView imageViewUserPhoto;
    TextView textViewUserName;
    FloatingActionButton fabPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        imageViewUserPhoto = findViewById(R.id.imageViewUserPhoto);
        textViewUserName = findViewById(R.id.textViewUsername);
        fabPhoto = findViewById(R.id.fabPhoto);

        registerForContextMenu(fabPhoto);

        firebaseAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater menuInflater = getMenuInflater();
        switch (v.getId()) {
            case R.id.fabPhoto:
                menuInflater.inflate(R.menu.photo_selector_menu, menu);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.settings_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case PICK_IMAGE_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {
                    Log.d("zxc2", data.getData().toString());

                    Glide.with(this).load(data.getData()).asBitmap().centerCrop().into(new BitmapImageViewTarget(imageViewUserPhoto) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                            roundedBitmapDrawable.setCircular(true);
                            imageViewUserPhoto.setImageDrawable(roundedBitmapDrawable);
                        }
                    });
                } else {
                    Toast.makeText(this, getString(R.string.unknown_error), Toast.LENGTH_SHORT).show();
                }
                break;
            case TAKE_PHOTO_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK && data != null && data.getExtras() != null && data.getExtras().get("data") != null) {
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                    assert bitmap != null;
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    Glide.with(this).load(stream.toByteArray()).asBitmap().centerCrop().into(new BitmapImageViewTarget(imageViewUserPhoto) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                            roundedBitmapDrawable.setCircular(true);
                            imageViewUserPhoto.setImageDrawable(roundedBitmapDrawable);
                        }
                    });
                } else {
                    Toast.makeText(this, getString(R.string.unknown_error), Toast.LENGTH_SHORT).show();
                }
                break;
            default:
        }
    }

    public void logout(MenuItem item) {
        Intent intentMain = new Intent(this, MainActivity.class);
        firebaseAuth.signOut();
        startActivity(intentMain);
        finish();
    }

    public void clickPhoto(View view) {
        openContextMenu(fabPhoto);
    }

    public void clickFromCamera(MenuItem item) {
        Intent intentTake = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intentTake, TAKE_PHOTO_REQUEST_CODE);
    }

    public void clickFromGallery(MenuItem item) {
        Intent intentGet = new Intent(Intent.ACTION_GET_CONTENT);
        intentGet.setType("image/*");
        Intent intentChooser = Intent.createChooser(intentGet, getString(R.string.str_select_photo));
        startActivityForResult(intentChooser, PICK_IMAGE_REQUEST_CODE);
    }

    public void editName(MenuItem item) {final EditText editTextUserName = new EditText(this);
        editTextUserName.setHint(getString(R.string.str_name));
        editTextUserName.setText(textViewUserName.getText().toString());
        editTextUserName.setSingleLine();
        editTextUserName.setFilters(new InputFilter[] {new InputFilter.LengthFilter(36)});
        editTextUserName.setImeOptions(EditorInfo.IME_ACTION_DONE);
        final AlertDialog alertDialog = new  AlertDialog.Builder(this)
                .setView(editTextUserName)
                .setPositiveButton(getString(R.string.str_ok), (dialog, which) -> textViewUserName.setText(editTextUserName.getText().toString()))
                .setNegativeButton(getString(R.string.str_cancel), (dialog, which) -> {
                })
                .show();
        editTextUserName.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                if (alertDialog.getWindow() != null) {
                    alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                }
            }
        });
        editTextUserName.requestFocus();
    }
}
