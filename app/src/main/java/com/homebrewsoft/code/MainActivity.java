package com.homebrewsoft.code;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GithubAuthProvider;
import com.google.firebase.auth.GoogleAuthProvider;

public class MainActivity extends Activity {
    private ConstraintLayout constraintLayoutMain;
    private TextView textViewInfo;
    private Button buttonInfo;
    private Button buttonJoin;
    private EditText editTextEmail;
    private EditText editTextPassword;
    private ProgressBar progressBar;

    private FirebaseAuth firebaseAuth;
    private int realB;
    private boolean headerVisible;
    final private int RC_SIGN_IN_GOOGLE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        constraintLayoutMain = findViewById(R.id.constraintLayoutMain);
        textViewInfo = findViewById(R.id.textViewInfo);
        buttonInfo = findViewById(R.id.buttonInfo);
        buttonJoin = findViewById(R.id.buttonJoin);
        editTextEmail = findViewById(R.id.editTextEmail);
        editTextPassword = findViewById(R.id.editTextPassword);
        progressBar = findViewById(R.id.progressBar);

        realB = 0;
        headerVisible = true;
        firebaseAuth = FirebaseAuth.getInstance();

        Uri uri = getIntent().getData();
        if (uri != null && uri.toString().startsWith(getString(R.string.github_redirect_uri))) {
            String code = uri.getQueryParameter("code");
            if (code != null) {
                progressBar.setVisibility(View.VISIBLE);
                postGitHub(code);
            }
        }

        constraintLayoutMain.addOnLayoutChangeListener((view, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom) -> {
            if (realB == 0) {
                realB = bottom;
            }
            if (realB != bottom) {
                hideHeader();
            } else {
                showHeader();
            }
        });

        hideHeader();
        showHeader();
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser user = firebaseAuth.getCurrentUser();
        if (user != null) {
            Intent intentHome = new Intent(MainActivity.this, HomeActivity.class);
            startActivity(intentHome);
            finish();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN_GOOGLE) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                GoogleSignInAccount account = result.getSignInAccount();
                assert account != null;
                AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
                signInWithCredential(credential);
            } else {
                Toast.makeText(this, "Authentication failed.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void postGitHub(String code) {
        Uri uri = new Uri.Builder()
                .scheme("https")
                .authority("github.com")
                .appendPath("login")
                .appendPath("oauth")
                .appendPath("access_token")
                .appendQueryParameter("client_id", getString(R.string.github_client_id))
                .appendQueryParameter("client_secret", getString(R.string.github_client_secret))
                .appendQueryParameter("code", code)
                .build();
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest request = new StringRequest(Request.Method.POST, uri.toString(), response -> {
            progressBar.setVisibility(View.GONE);
            if (response.startsWith("access_token")) {
                String token = response.split("[=&]")[1];
                AuthCredential credential = GithubAuthProvider.getCredential(token);
                signInWithCredential(credential);
            } else {
                Toast.makeText(MainActivity.this, R.string.str_error_github_authentication, Toast.LENGTH_SHORT).show();
            }
        }, error -> {
            progressBar.setVisibility(View.GONE);
            Toast.makeText(MainActivity.this, error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        });
        queue.add(request);
    }

    private void hideHeader() {
        if (!headerVisible) {
            return;
        }
        textViewInfo.setVisibility(View.GONE);
        buttonInfo.setVisibility(View.GONE);
        buttonJoin.setVisibility(View.GONE);
        headerVisible = false;
    }

    private void showHeader() {
        if (headerVisible) {
            return;
        }
        Animation alphaAnimation = new AlphaAnimation(0.00f, 1.00f);
        alphaAnimation.setDuration(500);
        alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                textViewInfo.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                showHeaderButtons();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

//        textViewInfo.animate().scaleY(0).start();
        textViewInfo.startAnimation(alphaAnimation);
        headerVisible = true;
    }

    private void showHeaderButtons() {
        Animation alphaAnimation = new AlphaAnimation(0.00f, 1.00f);
        alphaAnimation.setDuration(500);
        alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                buttonInfo.setVisibility(View.VISIBLE);
                buttonJoin.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        buttonInfo.startAnimation(alphaAnimation);
        buttonJoin.startAnimation(alphaAnimation);
    }

    public void clickLearnMore(View view) {
        Intent intentInfo = new Intent(MainActivity.this, InfoActivity.class);
        startActivity(intentInfo);
    }

    public void clickJoin(View view) {
        Intent intentJoin = new Intent(MainActivity.this, JoinActivity.class);
        startActivity(intentJoin);
    }

    public void clickLogin(View view) {
        if (editTextEmail.getText().length() == 0) {
            editTextEmail.setError("Email required");
            return;
        }
        if (editTextPassword.getText().length() == 0) {
            editTextPassword.setError("Password required");
            return;
        }
        progressBar.setVisibility(View.VISIBLE);
        firebaseAuth.signInWithEmailAndPassword(editTextEmail.getText().toString(), editTextPassword.getText().toString())
                .addOnCompleteListener(this, task -> {
                    progressBar.setVisibility(View.GONE);
                    if (task.isSuccessful()) {
                        Intent intentHome = new Intent(MainActivity.this, HomeActivity.class);
                        startActivity(intentHome);
                        finish();
                    } else {
                        Toast.makeText(MainActivity.this, R.string.str_incorrect_email_or_password, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void clickConnectGoogle(View view) {
        GoogleSignInOptions options = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestProfile()
                .build();
        GoogleSignInClient client = GoogleSignIn.getClient(this, options);
        Intent signInIntent = client.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN_GOOGLE);
    }

    public void clickConnectGitHub(View view) {
        progressBar.setVisibility(View.VISIBLE);
        Uri uriGitHub = new Uri.Builder()
                .scheme("http")
                .authority("github.com")
                .appendPath("login")
                .appendPath("oauth")
                .appendPath("authorize")
                .appendQueryParameter("client_id", getString(R.string.github_client_id))
                .appendQueryParameter("scope", "user:email")
                .build();

        Intent intentSigInGitHub = new Intent(Intent.ACTION_VIEW, uriGitHub);
        startActivity(intentSigInGitHub);
    }

    private void signInWithCredential(AuthCredential credential) {
        progressBar.setVisibility(View.VISIBLE);
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, task -> {
                    progressBar.setVisibility(View.GONE);
                    if (task.isSuccessful()) {
                        Intent intentHome = new Intent(MainActivity.this, HomeActivity.class);
                        startActivity(intentHome);
                        finish();
                    } else {
                        Toast.makeText(MainActivity.this, task.getException().getLocalizedMessage(),
                                Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
