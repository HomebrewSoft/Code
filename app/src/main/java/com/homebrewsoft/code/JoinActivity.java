package com.homebrewsoft.code;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class JoinActivity extends AppCompatActivity {
    private FirebaseAuth firebaseAuth;

    EditText editTextName;
    EditText editTextEmail;
    EditText editTextPassword;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join);

        editTextName = findViewById(R.id.editTextName);
        editTextEmail = findViewById(R.id.editTextEmail);
        editTextPassword = findViewById(R.id.editTextPassword);
        progressBar = findViewById(R.id.progressBar);

        firebaseAuth = FirebaseAuth.getInstance();

        editTextName.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                isValidName();
            }
        });

        editTextEmail.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                isValidEmail();
            }
        });

        editTextPassword.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                isValidPassword();
            }
        });
    }

    public void clickAppTerms(View view) {
        Intent intentAppTerms = new Intent(Intent.ACTION_VIEW, Uri.parse(this.getString(R.string.url_terms_site)));
        startActivity(intentAppTerms);
    }

    public void clickPrivacyPolicy(View view) {
        Intent intentAppTerms = new Intent(Intent.ACTION_VIEW, Uri.parse(this.getString(R.string.url_privacy_policy)));
        startActivity(intentAppTerms);
    }

    private boolean isValidName() {
        String name = editTextName.getText().toString();
        if (name.trim().length() == 0) {
            editTextName.setError(editTextName.getHint() + " required");
            return false;
        }
//        TODO make more validations (valid chars)
        return true;
    }

    private boolean isValidEmail() {
        String email = editTextEmail.getText().toString();
        if (email.length() == 0) {
            editTextEmail.setError(editTextEmail.getHint() + " required");
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            editTextEmail.setError(editTextEmail.getHint() + " invalid");
            return false;
        }
//        TODO make more validations (valid chars)
        return true;
    }

    private boolean isValidPassword() {
        String password = editTextPassword.getText().toString();
        if (password.length() == 0) {
            editTextPassword.setError(editTextPassword.getHint() + " required");
            return false;
        } else if (password.length() < 1) {
            editTextPassword.setError("Min 6 characters");
            return false;
        }
//        TODO make more validations (valid chars)
        return true;
    }

    public void clickJoin(View view) {
        progressBar.setVisibility(View.VISIBLE);
        if (!isValidName() || !isValidEmail() || !isValidPassword()) {
            return;
        }
        final String name = editTextName.getText().toString();
        final String email = editTextEmail.getText().toString();
        final String password = editTextPassword.getText().toString();
        firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        DatabaseManager.setUser(name, email).addOnCompleteListener(task1 -> {
                            if (task1.isSuccessful()) {
                                Intent intentHome = new Intent(JoinActivity.this, HomeActivity.class);
                                startActivity(intentHome);
                                finish();
                            } else {
                                Toast.makeText(JoinActivity.this, task1.getException().getLocalizedMessage(),
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else {
                        Toast.makeText(JoinActivity.this, task.getException().getLocalizedMessage(),
                                Toast.LENGTH_SHORT).show();
                    }
                    progressBar.setVisibility(View.GONE);
                });
    }
}
